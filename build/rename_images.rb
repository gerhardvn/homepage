def search_high_number(numbered_files)
  if numbered_files != nil
    numbered_files = numbered_files.map {|file| file.match(  /\.\/\.\.\/assets\/(\d{1,3})/).captures[0].to_i}
    numbered_files.max_by {|i| i}  
  else
    0 #no numbered files found
  end
end

def rename_files(unnumbered_files, highest_number)
  if unnumbered_files != nil
    unnumbered_files.each do |file|
      highest_number += 1
      rename = "./../assets/#{highest_number}#{File.extname(file)}"
      puts "#{file} -> #{rename}"
      File.rename(file,rename)
    end
  end
end

files = Dir["./../assets/*"]

numbered_files = files.select {|file| file =~ /\.\/\.\.\/assets\/\d{1,3}\./}
unnumbered_files = files.reject {|file| file =~/\.\/\.\.\/assets\/\d{1,3}\./}  

highest_number = search_high_number(numbered_files)

rename_files(unnumbered_files, highest_number)
